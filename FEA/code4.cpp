/*
*
* Code to solve Eigenvalue Problem 
* using 1st order Lagrange Shape functions
* K d�u/dx� = lambda u(x)
*
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include <algorithm>
#include "Eigen/Dense"
#include "Eigen/Eigen"


using namespace Eigen;
using namespace std;


int main() {

    double k  = 1.0;

    int n_elem  = 32.0; //8.0, 16.0, 32.0;
    int n_nodes = n_elem+1;
    double length  = 1.0;
    double h       = length/ (n_nodes-1);

//element stiffness matrix
    MatrixXd Ae= MatrixXd::Zero(2,2);
    Ae.row(0) << 1.0, -1.0;
    Ae.row(1) << -1.0, 1.0;
    Ae *= k/h;

//element mass matrix
MatrixXd Be= MatrixXd::Zero(2,2);
    Be.row(0) << 2.0, 1.0;
    Be.row(1) << 1.0, 2.0;
    Be *= 2*h/3;

//Global matrices assembly
    MatrixXd A_mat   = MatrixXd::Zero(n_nodes,n_nodes);
    MatrixXd B_mat   = MatrixXd::Zero(n_nodes,n_nodes);
    VectorXd X_vec   = VectorXd::Zero(n_nodes);
    VectorXd indices = VectorXd::Zero(2);

//Global stiffness and force vector
    for(int i_elem=0; i_elem<n_elem;i_elem++){

        MatrixXd A_mat_old_e                = A_mat.block<2,2>(i_elem,i_elem);
        A_mat_old_e                         = A_mat_old_e + Ae;
        A_mat.block<2,2>(i_elem,i_elem) = A_mat_old_e;

      	MatrixXd B_mat_old_e                = B_mat.block<2,2>(i_elem,i_elem);
        B_mat_old_e                         = B_mat_old_e + Be;
        B_mat.block<2,2>(i_elem,i_elem) = B_mat_old_e;
    }


// nodal locations to plot analytical solution
    VectorXd x_loc2 = VectorXd::LinSpaced(600,0,1);;

//applying boundary conditions
    //getting the submatrices corresponding to uu and ud components
    MatrixXd A_s = A_mat.bottomRightCorner((n_nodes-1), (n_nodes-1));
    MatrixXd A_sub = A_s.topLeftCorner((n_nodes-2), (n_nodes-2));
	MatrixXd B_s = B_mat.bottomRightCorner((n_nodes-1), (n_nodes-1));
	MatrixXd B_sub = B_s.topLeftCorner((n_nodes-2), (n_nodes-2));

// solving the EVP
    GeneralizedSelfAdjointEigenSolver<MatrixXd> ges(A_sub,B_sub);
    //saving the numerical eigenvalues
    VectorXd eig_val= VectorXd::Zero(n_nodes-2);
    eig_val= ges.eigenvalues().real();

// nodal locations to plot analytical solution
    VectorXd X_loc2 = VectorXd::LinSpaced(600,0,1);
// EVP analytical solution 
	MatrixXd X_anl   = MatrixXd::Zero(600,4);
	VectorXd eig_anl(4);
	for(int i=0; i<X_loc2.size(); i++){
		for( int n=0; n<4; n++){
    		X_anl(i,n) = sin((n+1)*M_PI*X_loc2(i));
    		eig_anl(n) = pow((n+1)*M_PI, 2);
		}
	}

// FEM error Analysis
   // error of 1st eigval for m
   double lg_er1 = log10(abs(eig_anl(0)-eig_val(0) ));
   // error of 2nd eigval
   double lg_er2= log10(abs(eig_anl(1)-eig_val(1) ));
   // error of 3rd eigval
   double lg_er3= log10(abs(eig_anl(2)-eig_val(2) )); 
   // error of 4th eigval
   double lg_er4= log10(abs(eig_anl(3)-eig_val(3) ));

    
//saving data
    ofstream file("1eigloger.txt",std::ios_base::app); 
	//Saving 1/#dof and eigvals errors 
    file << /*log10(0.1111)log10(0.0588)log10(0.0303)*/log10(0.0154) << std::setw(20) << lg_er1 << std::setw(20) << lg_er2 << std::setw(20) << lg_er3 <<std::setw(20) << lg_er4 << endl;

    file.close();
}
 
