/* 
 * This code solves the following differential equation 
 * - d�u/dx� = 2,  du(1)/dx = -1, x in [0,1]
 * using a uniformly spaced mesh of 2 elements
 * with second-order Lagrange shape functions.
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include "Eigen/Dense"
#include "Eigen/Eigen"


using namespace Eigen;
using namespace std;


int main() {

    //element stiffness matrix
    MatrixXd ke= MatrixXd::Zero(3,3);
    ke.row(0) << 1.1667, -1.3333, 0.1667;
    ke.row(1) << -1.3333, 2.6667, -1.3333;
    ke.row(2) << 0.1667, -1.3333, 1.1667;
    //Global stiffness
    MatrixXd K= MatrixXd::Zero(5,5);
    K.block<3,3>(0,0) = ke;
    K.block<3,3>(2,2) += ke;
    //applying boundary condition on K
    MatrixXd bc= MatrixXd::Zero(1,5);
    K.row(0) = bc;
    K(0.0)   = 1;
    MatrixXd K_mat = 4*K; // (2/h)*K where h=1/2

    //force vector
    VectorXd fe(3);
    fe<<0.3334, 1.3334, 0.3334;
    //Global force vector
    VectorXd F= VectorXd::Zero(5); //ArrayXf F = ArrayXf::Zero(5);
    F.head(3) =fe;
    F.tail(3) +=fe;
    VectorXd F_vec = 0.5*F; // (2*h/2)*F
    //applying boundary condition on K
    F_vec(0)  =  0;  //u(0)=0
    F_vec(4) += -1;  //du(l)/dx=-1

    //solving for x
    VectorXd X_vec = K_mat.colPivHouseholderQr().solve(F_vec);

 //saving data
    ofstream file1("numsol.txt");
    ofstream file2("anlsol.txt");
    if(!file1.is_open() && !file2.is_open()){
        cout<<"unable to open files"<<endl;
    }else{
        //Saving numerical solution
        VectorXd x_loc1 = VectorXd::LinSpaced(5,0,1);
        for(int i=0; i<x_loc1.size();i++){
            X_vec(i) =(x_loc1(i)-pow(x_loc1(i),2));
            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << endl;
        }
        //Saving analytical solution
        VectorXd x_loc2 = VectorXd::LinSpaced(100,0,1);
        VectorXd X_anl(100);
        for(int i=0; i<x_loc2.size();i++){
            X_anl(i) =(x_loc2(i)-pow(x_loc2(i),2));     // Analytical solution of the DE is x-x^2
            file2 << x_loc2(i) << std::setw(20) << X_anl(i) << endl;
        }
        file1.close();
        file2.close();
    }
}
