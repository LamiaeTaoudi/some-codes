/*
 *  This code solves the differential equation of the form Kd^4u(x)/dx=f 
 *  where 0<x<1 and u(0)=u(1)=0 and d�u(0)/dx�=d�u(1)/dx�=0
 *  a) using 1 second order lagrange element 
 *  b) using 1 third order lagrange element
 */
 
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "Eigen/Dense"
#include "Eigen/Eigen"


using namespace Eigen;
using namespace std;


int main() {
	
	//element stiffness matrix
    MatrixXd ke1= MatrixXd::Zero(3,3);
    ke1.row(0) <<   16, -32,  16;
    ke1.row(1) <<  -32,  64, -32;
    ke1.row(2) <<   16, -32,  16;
    
	//element force vector
    VectorXd fe1(3);
    fe1<<0.5, 2, 0.5;
    
	//applying boundary conditions
    //force vector
    fe1(0) = g;
    fe1(2) = g;
    
    //sol
    VectorXd X_vec1= VectorXd::Zero(3);
    X_vec1(0) = u0;
    X_vec1(1) = fe1(1)/ke1(1,1);
    X_vec1(2) = u0;

// b) 3rd order lagrange 
	//element stiffness matrix
    MatrixXd kee2 = MatrixXd::Zero(4,4);
    kee2.row(0) <<     81, -202.5,    162,  -40.5;
    kee2.row(1) << -202.5,    567, -526.5,    162;
    kee2.row(2) <<    162, -526.5,    567, -202.5;
    kee2.row(3) <<  -40.5,    162, -202.5,     81;
  
	//element force vector
    VectorXd fe2(4);
    fe2<<0.375, 1.125, 1.125, 0.375;

    //applying boundary conditions
    //force vector
    fe2(0) = g;
    fe2(3) = g;
    //sol
    VectorXd X_vec2= VectorXd::Zero(4);
    X_vec2(0) = u0;
    X_vec2(3) = u0;

    //getting the submatrices corresponding to uu and ud components
    MatrixXd K_mat_u  = kee2.bottomRightCorner(3,3);
    MatrixXd K_mat_uu = K_mat_u.topLeftCorner(2,2);
    
    VectorXd F_vec_u  = fe2.segment(1,2);
   
    VectorXd X_vec_u = K_mat_uu.lu().solve(F_vec_u); 
   	X_vec2.segment(1,2) = X_vec_u;

//saving data
    ofstream file1("numsol1.txt");
    ofstream file2("numsol2.txt");
    ofstream file3("anlsol.txt");
    
	// nodal locations to plot numerical solution
    VectorXd x_loc1 = VectorXd::LinSpaced(3,0,1);
	VectorXd x_loc2 = VectorXd::LinSpaced(4,0,1);
	// nodal locations to plot analytical solution
    VectorXd x_loca = VectorXd::LinSpaced(200,0,1);
    
	//Saving numerical solution 1
    for(int i=0; i<x_loc1.size();i++){
		file1 << x_loc1(i) << std::setw(20) << X_vec1(i) << endl;
    }
    //Saving numerical solution 1
    for(int i=0; i<x_loc2.size();i++){
        file2 << x_loc2(i) << std::setw(20) << X_vec2(i) << endl;
	}
    //Saving analytical solution
    VectorXd X_anl(200);
    for(int i=0; i<x_loca.size();i++){
        X_anl(i) =std::pow(x_loca(i),4)/8-std::pow(x_loca(i),3)/4+x_loca(i)/8;  // Analytical solution of the DE is x^4/8 - x^3/4 +x/8
        file3 << x_loca(i) << std::setw(20) << abs(X_anl(i)) << endl;
    }
    file1.close();
	file2.close();
	file3.close();
}
