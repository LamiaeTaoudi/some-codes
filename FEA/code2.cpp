/* 
 * This code solves the following differential equation 
 * - d�u/dx� = 2,  du(1)/dx = -1, x in [0,1]
 * using a uniformly spaced mesh of m elements, where m = 4; 8; 16; 32
 * with second-order Lagrange shape functions.
 *
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "Eigen/Dense"
#include "Eigen/Eigen"


using namespace Eigen;
using namespace std;


int main() {

    double f  = 2.0;
    double g  = -1.0;
    double u0  = 0.0;

    int n_elem  = 32; //4.0; //8.0, 16.0, 32.0;
    int n_nodes = 2*n_elem+1;
    double length  = 1.0;
    double h       = length/ (n_nodes-1);

//element stiffness matrix
    MatrixXd ke= MatrixXd::Zero(3,3);
    ke.row(0) << 1.166666, -1.333333, 0.166666;
    ke.row(1) << -1.333333, 2.666666, -1.333333;
    ke.row(2) << 0.166666, -1.333333, 1.166666;
    ke *= 1/h;

//element force vector
    VectorXd fe(3);
    fe<<0.333333, 1.333333, 0.333333;
    fe *=f*h;

//Global matrices assembly
    MatrixXd K_mat   = MatrixXd::Zero(n_nodes,n_nodes);
    VectorXd F_vec   = VectorXd::Zero(n_nodes);
    VectorXd X_vec   = VectorXd::Zero(n_nodes);
    VectorXd indices = VectorXd::Zero(3);

//Global stiffness and force vector
    for(int i_elem=0; i_elem<n_elem;i_elem++){

        MatrixXd K_mat_old_e            = K_mat.block<3,3>(2*i_elem,2*i_elem);
        K_mat_old_e                     = K_mat_old_e + ke;
        K_mat.block<3,3>(2*i_elem,2*i_elem) = K_mat_old_e;

        VectorXd F_vec_old_e               = F_vec.segment<3>(2*i_elem);
        F_vec_old_e                         = F_vec_old_e + fe;
        F_vec.segment<3>(2*i_elem) = F_vec_old_e;
    }

// nodal locations to plot numerical solution
    VectorXd x_loc1 = VectorXd::LinSpaced(n_nodes,0,1);

// nodal locations to plot analytical solution
    VectorXd x_loc2 = VectorXd::LinSpaced(600,0,1);;

//applying boundary conditions
    //force vector
    F_vec(n_nodes-1) += g;
    //sol
    X_vec(0)  = u0;

    //VectorXd X_vec_anl = -f*0.5*pow(x_loc2,2)+(g+f*length)*x_loc2+u0;

    //getting the submatrices corresponding to uu and ud components
    MatrixXd K_mat_uu = K_mat.bottomRightCorner((n_nodes-1), (n_nodes-1));
    VectorXd K_mat_ud = K_mat.col(0).segment(1,(n_nodes-1));

    VectorXd F_vec_d  = F_vec.head(1);
    VectorXd F_vec_u  = F_vec.segment(1,(n_nodes-1));
    VectorXd X_vec_d  = X_vec.head(1);

    //solve the system
    VectorXd X_vec_u = K_mat_uu.lu().solve(F_vec_u-K_mat_ud*X_vec_d);
    X_vec.segment(1,(n_nodes-1)) = X_vec_u;

    //saving data
    ofstream file1("numsol.txt");
    ofstream file2("anlsol.txt");
    if(!file1.is_open() && !file2.is_open()){
        cout<<"unable to open files"<<endl;
    }else{
        //Saving numerical solution
        for(int i=0; i<x_loc1.size();i++){
            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << endl;
        }
        //Saving analytical solution
       VectorXd X_anl(600);
        for(int i=0; i<x_loc2.size();i++){
            X_anl(i) =-f*0.5*std::pow(x_loc2(i),2)+(g+f*length)*x_loc2(i)+u0;  // Analytical solution of the DE is x-x^2
            file2 << x_loc2(i) << std::setw(20) << X_anl(i) << endl;
        }
        file1.close();
        file2.close();
    }

}

