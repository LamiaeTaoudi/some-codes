/*
 *
 * Code to solve Eigenvalue Problem 
 * using Hermite Shape functions
 * EI d^4w/dx^4 =  omg� rhoA w(x)
 * 
 * and to solve transient problem 
 * rhoA d�w/dt� + EI d^4w/dx^4 =  f(t)
 * using Newmark method for time integration 
 */

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "Eigen/Dense"
#include "Eigen/Eigen"
#include "Eigen/Eigenvalues"


using namespace Eigen;
using namespace std;


int main() {

	double EI   = 20.25;
	double rhoA = 4.05;
	double beta = 0.25;
	double gama = 0.5;

    int n_elem     = 16.0;
    int n_nodes    = n_elem+1;
    double length  = 0.3;
    double h       = length/ (n_nodes-1);
    
    // Element stiffness matrix
	MatrixXd ke= MatrixXd::Zero(4,4);
	ke.row(0) <<  6,    -6,   3*h,   3*h;
	ke.row(1) << -6,     6,  -3*h,  -3*h;
	ke.row(2) << 3*h, -3*h, 2*h*h,   h*h;
	ke.row(3) << 3*h, -3*h,   h*h, 2*h*h;
	ke *= 2*EI/pow(h,3); 

	// Element mass matrix
	MatrixXd be= MatrixXd::Zero(4,4);
	be.row(0) <<    0.7429,    0.2571,    0.1048*h,   -0.0619*h;
	be.row(1) <<    0.2571,    0.7429,    0.0619*h,   -0.1048*h;
	be.row(2) <<  0.1048*h,  0.0619*h,  0.0190*h*h,  0.0143*h*h;
	be.row(3) << -0.0619*h, -0.1048*h,  0.0143*h*h,  0.0190*h*h;
	be *= rhoA*h/2; 

	// Global matrices assembly
	MatrixXd K_mat   = MatrixXd::Zero(2*n_nodes,2*n_nodes);
	MatrixXd B_mat   = MatrixXd::Zero(2*n_nodes,2*n_nodes);
	VectorXd X_vec   = VectorXd::Zero(2*n_nodes);
	
	// Global stiffness and mass matrices
	for(int i_elem=0; i_elem <n_elem ;i_elem++){

		MatrixXd K_mat_old_e1            = K_mat.block<2,2>(i_elem,i_elem);
	    K_mat_old_e1                     = K_mat_old_e1 + ke.topLeftCorner(2,2);
	    K_mat.block<2,2>(i_elem,i_elem)  = K_mat_old_e1;
	    
	    MatrixXd K_mat_old_e2            = K_mat.block<2,2>(i_elem,i_elem+n_nodes);
	    K_mat_old_e2                     = K_mat_old_e2 + ke.topRightCorner(2,2);
	    K_mat.block<2,2>(i_elem,i_elem+n_nodes) = K_mat_old_e2;
	        
	    MatrixXd K_mat_old_e3            = K_mat.block<2,2>(i_elem+n_nodes,i_elem);
	    K_mat_old_e3                     = K_mat_old_e3 + ke.bottomLeftCorner(2,2);
	    K_mat.block<2,2>(i_elem+n_nodes,i_elem) = K_mat_old_e3;
	        
	    MatrixXd K_mat_old_e4            = K_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes);
	    K_mat_old_e4                     = K_mat_old_e4 + ke.bottomRightCorner(2,2);
	    K_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes) = K_mat_old_e4;
	   
	    MatrixXd B_mat_old_e1            = B_mat.block<2,2>(i_elem,i_elem);
	    B_mat_old_e1                     = B_mat_old_e1 + be.topLeftCorner(2,2);
	    B_mat.block<2,2>(i_elem,i_elem)  = B_mat_old_e1;
	
	    MatrixXd B_mat_old_e2            = B_mat.block<2,2>(i_elem,i_elem+n_nodes);
	    B_mat_old_e2                     = B_mat_old_e2 + ke.topRightCorner(2,2);
	    B_mat.block<2,2>(i_elem,i_elem+n_nodes) = B_mat_old_e2;
	
	    MatrixXd B_mat_old_e3            = B_mat.block<2,2>(i_elem+n_nodes,i_elem);
	    B_mat_old_e3                     = B_mat_old_e3 + be.bottomLeftCorner(2,2);
	    B_mat.block<2,2>(i_elem+n_nodes,i_elem) = B_mat_old_e3;
	        
	    MatrixXd B_mat_old_e4            = B_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes);
	    B_mat_old_e4                     = B_mat_old_e4 + be.bottomRightCorner(2,2);
		B_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes) = B_mat_old_e4;
	}
	
	// Applying boundary conditions
    K_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	K_mat(0,0)         = 1;
	K_mat.row(n_nodes-1)       = VectorXd::Zero(2*n_nodes); //u1
	K_mat(n_nodes-1,n_nodes-1) = 1;
	B_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	B_mat(0,0)         = 1;
	B_mat.row(n_nodes-1)       = VectorXd::Zero(2*n_nodes); //u1
	B_mat(n_nodes-1,n_nodes-1) = 1;


	/******* Question (1) ********/
    
	// Solving the Eigenproblem 
    GeneralizedSelfAdjointEigenSolver<MatrixXd> ges(K_mat,B_mat);
    VectorXd eig_val= ges.eigenvalues().real();
	// calculating the lowest natural frequency
	double omg = pow(eig_val(2), 0.5);
    cout<<"omg" <<endl <<omg <<endl;
    // scaling the corresponding eigenvector   
    VectorXd V1 = ges.eigenvectors().col(2).real().segment(1,(n_nodes));
    VectorXd V1new= VectorXd::Zero(n_nodes);
    V1new = V1.array().abs()/V1.array().abs().maxCoeff();
    // saving results 	
    VectorXd x_loc = VectorXd::LinSpaced(n_nodes,0,0.3);
    ofstream file0("eig.txt");
	file0 << "x" << std::setw(20) << "eig" << endl;
	for(int i=0; i<x_loc.size() ;i++){
	    file0 << x_loc(i) << std::setw(20) << V1new(i) << endl;
	}
	file0.close();
	

	/******* Question (2) ********/
    
	double T = 2*M_PI/omg;
    VectorXd w(2*n_nodes);
    for (int i=0; i<x_loc.size(); i++){
		w(i) = sin(M_PI*x_loc(i)/length);
		w(i+n_nodes) = M_PI*cos(M_PI*x_loc(i)/length)/length;
	}
	VectorXd dw_dt   = VectorXd::Zero(2*n_nodes);
	
	// solving the problem 
	VectorXd d2w_dt = B_mat.lu().solve(-K_mat*w);
	cout<<"d2w_dt" <<endl <<d2w_dt <<endl;
    
    VectorXd m(5);
	m << 2.0, 4.0, 8.0, 16.0, 32.0;
	MatrixXd K_new = MatrixXd::Zero(2*n_nodes,2*n_nodes);
	for (int n=0; n<m.size(); n++){
		K_new = B_mat/(beta*pow(T/m(n),2));
		K_new += K_mat;
		double dT=T/m(n); 
		VectorXd time = VectorXd::LinSpaced((10*T/dT),0,10*T);
	
		for (int j=0; j<time.size(); j++){
		VectorXd F         = B_mat/(beta*pow(dT,2))*(w+(0.5*d2w_dt*pow(dT,2))-(beta*d2w_dt*pow(dT,2))+dw_dt*dT);
		VectorXd w_new      = K_new.lu().solve(F);
		VectorXd d2w_dt_new = 1/(beta*pow(dT,2))*(w_new-w-(0.5-beta)*d2w_dt*pow(dT,2)-dw_dt*dT);
		VectorXd dw_dt_new  = dw_dt+(1-gama)*d2w_dt*dT+gama*d2w_dt_new*dT;
		
		dw_dt      =  dw_dt_new;
		d2w_dt     =  d2w_dt_new;
		w          = w_new;
		}
		
		// saving the solution 
		if (m(n)==2.0){
		ofstream file1("wn2.txt");
		file1 << "x" << std::setw(20) << "w" << endl;
		for(int i=0; i<x_loc.size() ;i++)
	            file1 << x_loc(i) << std::setw(20) << w(i) << endl;
	 	file1.close();
		}
	 	if (m(n)==4.0){
		ofstream file2("wn4.txt");
		file2 << "x" << std::setw(20) << "w" << endl;
		for(int i=0; i<x_loc.size() ;i++)
	            file2 << x_loc(i) << std::setw(20) << w(i) << endl;
	 	file2.close();
		}
	 	if (m(n)==8.0){
		ofstream file3("wn8.txt");
		file3 << "x" << std::setw(20) << "w" << endl;
		for(int i=0; i<x_loc.size() ;i++)
	            file3 << x_loc(i) << std::setw(20) << w(i) << endl;
	 	file3.close();
	 	}
	 	if (m(n)==16.0){
		ofstream file4("wn16.txt");
		file4 << "x" << std::setw(20) << "w" << endl;
		for(int i=0; i<x_loc.size() ;i++)
	            file4 << x_loc(i) << std::setw(20) << w(i) << endl;
	 	file4.close();
	 	}
	 	if (m(n)==32.0){
		ofstream file5("wn32.txt");
		file5 << "x" << std::setw(20) << "w" << endl;
		for(int i=0; i<x_loc.size() ;i++)
	            file5 << x_loc(i) << std::setw(20) << w(i) << endl;
	 	file5.close();
		}
	}

}
