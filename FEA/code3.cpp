/*
*
* Code to solve Eigenvalue Problem 
* using 1st order Lagrange Shape functions
* K d�u/dx� = lambda u(x)
*
*/

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "Eigen/Dense"
#include "Eigen/Eigen"
#include "Eigen/Eigenvalues"


using namespace Eigen;
using namespace std;


int main() {

    double k  = 1.0;

    int n_elem     = 32.0; //8.0, 16.0, 32.0;
    int n_nodes    = 2*n_elem+1;
    double length  = 1.0;
    double h       = length/ (n_nodes-1);

//element stiffness matrix
    MatrixXd Ae= MatrixXd::Zero(3,3);
    Ae.row(0) << 1.166666, -1.333333, 0.166666;
    Ae.row(1) << -1.333333, 2.666666, -1.333333;
    Ae.row(2) << 0.166666, -1.333333, 1.166666;
    Ae *= k/h;

//element mass matrix
    MatrixXd Be= MatrixXd::Zero(3,3);
    Be.row(0) << 0.266666, 0.133333, -0.066666;
    Be.row(1) << 0.1333333, 1.066666, 0.133333;
    Be.row(2) << -0.066666, 0.133333, 0.266666;
    Be *= h;

//Global matrices assembly
    MatrixXd A_mat   = MatrixXd::Zero(n_nodes,n_nodes);
    MatrixXd B_mat   = MatrixXd::Zero(n_nodes,n_nodes);
    VectorXd X_vec   = VectorXd::Zero(n_nodes);
    VectorXd indices = VectorXd::Zero(3);

//Global stiffness and force vector
    for(int i_elem=0; i_elem<n_elem;i_elem++){

        MatrixXd A_mat_old_e                = A_mat.block<3,3>(2*i_elem,2*i_elem);
        A_mat_old_e                         = A_mat_old_e + Ae;
        A_mat.block<3,3>(2*i_elem,2*i_elem) = A_mat_old_e;

        MatrixXd B_mat_old_e                = B_mat.block<3,3>(2*i_elem,2*i_elem);
        B_mat_old_e                         = B_mat_old_e + Be;
        B_mat.block<3,3>(2*i_elem,2*i_elem) = B_mat_old_e;
    }
//applying boundary conditions
    //getting the submatrices corresponding to uu and ud components
    MatrixXd A_s = A_mat.bottomRightCorner((n_nodes-1), (n_nodes-1));
    MatrixXd A_sub = A_s.topLeftCorner((n_nodes-2), (n_nodes-2));
    MatrixXd B_s = B_mat.bottomRightCorner((n_nodes-1), (n_nodes-1));
    MatrixXd B_sub = B_s.topLeftCorner((n_nodes-2), (n_nodes-2));
    
// solving the EVP
    GeneralizedSelfAdjointEigenSolver<MatrixXd> ges(A_sub,B_sub);
    //saving the numerical eigenvalues
    VectorXd eig_val= VectorXd::Zero(n_nodes-2);
    eig_val= ges.eigenvalues().real();
/*    
// scaling the 1st eigenvectors
    VectorXd V1= VectorXd::Zero(n_nodes);
    V1.segment(1,(n_nodes-2)) = ges.eigenvectors().col(0).real();
    VectorXd V1new= VectorXd::Zero(n_nodes);
    V1new = V1.array().abs()/V1.array().abs().maxCoeff();
    
// scaling of 2nd Eigenvector
    VectorXd V2up= VectorXd::Zero(0.5*(n_nodes));
    V2up.segment(1,(n_nodes-2)*0.5) = ges.eigenvectors().col(1).real().head((n_nodes-2)*0.5);
    VectorXd V2newup= VectorXd::Zero(0.5*(n_nodes));
    V2newup = V2up.array().abs()/V2up.array().abs().maxCoeff();

    VectorXd V2dw= VectorXd::Zero(0.5*n_nodes+1);
    V2dw.segment(1, (n_nodes-2)*0.5) = ges.eigenvectors().col(1).real().tail((n_nodes-2)*0.5);
   VectorXd V2newdw= VectorXd::Zero(0.5*n_nodes+1);
    V2newdw = -V2dw.array().abs()/V2dw.array().abs().maxCoeff();

    VectorXd V2new= VectorXd::Zero(n_nodes);
    V2new.head(n_nodes*0.5) = V2newup;
    V2new.tail(n_nodes*0.5+1) = V2newdw;

// scaling of 3rd Eigenvector 
	VectorXd V3up= VectorXd::Zero((0.3*n_nodes+2));
    V3up.segment(1,(n_nodes*0.3+1)) = ges.eigenvectors().col(2).real().head((n_nodes*0.3+1));
    VectorXd V3newup= VectorXd::Zero((0.3*n_nodes+2));
    V3newup = V3up.array().abs()/V3up.array().abs().maxCoeff();
    
    VectorXd V3dw= VectorXd::Zero((n_nodes/3));
    V3dw.segment(0,(n_nodes/3)) = ges.eigenvectors().col(2).real().segment(((n_nodes/3)-1),((n_nodes/3)));
    VectorXd V3newdw= VectorXd::Zero((n_nodes/3));
    V3newdw = -V3dw.array().abs()/V3dw.array().abs().maxCoeff();
    
 	VectorXd V3up2= VectorXd::Zero((n_nodes/3));	
    V3up2.segment(0,(n_nodes/3-1)) = ges.eigenvectors().col(2).real().tail((n_nodes/3-1));
    VectorXd V3newup2= VectorXd::Zero((n_nodes/3));
    V3newup2 = V3up2.array().abs()/V3up2.array().abs().maxCoeff();
  
  	VectorXd V3new= VectorXd::Zero(n_nodes);
    V3new.head((n_nodes/3)) = V3newup;
    V3new.segment((n_nodes/3),(n_nodes/3)) = V3newdw;
    V3new.tail((n_nodes/3)) = V3newup2; 
	
// scaling of the 4th eigenvector
    VectorXd V4up= VectorXd::Zero(0.25*(n_nodes));
    V4up.segment(1,(n_nodes-2)*0.25) = ges.eigenvectors().col(3).real().head((n_nodes-2)*0.25);
    VectorXd V4newup= VectorXd::Zero(0.25*(n_nodes));
    V4newup = V4up.array().abs()/V4up.array().abs().maxCoeff();

    VectorXd V4up2= VectorXd::Zero(0.25*(n_nodes));
    V4up2.segment(1,(n_nodes-2)*0.25) = ges.eigenvectors().col(3).real().segment((n_nodes-2)*0.25+1,(n_nodes-2)*0.25);
    VectorXd V4newup2= VectorXd::Zero(0.25*(n_nodes));
    V4newup2 = -V4up2.array().abs()/V4up.array().abs().maxCoeff();
    
    VectorXd V4dw= VectorXd::Zero(0.25*n_nodes);
    V4dw.segment(1, (n_nodes-2)*0.25) = ges.eigenvectors().col(3).real().segment((n_nodes-2)*0.5+1,(n_nodes-2)*0.25);
    VectorXd V4newdw= VectorXd::Zero(0.5*n_nodes+1);
    V4newdw = V4dw.array().abs()/V4up.array().abs().maxCoeff();

    VectorXd V4dw2= VectorXd::Zero(0.25*n_nodes);
    V4dw2.segment(1, (n_nodes-2)*0.25) = ges.eigenvectors().col(3).real().tail((n_nodes-2)*0.25);
    VectorXd V4newdw2= VectorXd::Zero(0.5*n_nodes+1);
    V4newdw2 = -V4dw.array().abs()/V4up.array().abs().maxCoeff();

    VectorXd V4new= VectorXd::Zero(n_nodes);
    V4new.head(n_nodes*0.25) = V4newup;
    V4new.segment(n_nodes*0.25,n_nodes*0.25) = V4newup2;
    V4new.segment(n_nodes*0.5,n_nodes*0.25) = V4newdw;
    V4new.segment(n_nodes*0.75,n_nodes*0.25) = V4newdw2;
*/
// EVP analytical solution
    // nodal locations to plot analytical solution
    VectorXd X_loc2 = VectorXd::LinSpaced(600,0,1);
    MatrixXd X_anl   = MatrixXd::Zero(600,4);
    VectorXd eig_anl(4);
    for(int i=0; i<X_loc2.size(); i++){
        for( int n=0; n<4; n++){
            X_anl(i,n) = sin((n+1)*M_PI*X_loc2(i));
            eig_anl(n) = pow((n+1)*M_PI, 2);
        }
    }
 cout << "eig_anal"<<eig_anl<< endl;
 cout << "eig_val" << eig_val<< endl;
// FEM error Analysis
   // error of 1st eigval for m
   double lg_er1 = log10(abs(eig_anl(0)-eig_val(0) ));
   // error of 2nd eigval
   double lg_er2= log10(abs(eig_anl(1)-eig_val(1) ));
   // error of 3rd eigval
   double lg_er3= log10(abs(eig_anl(2)-eig_val(2) )); 
   // error of 4th eigval
   double lg_er4= log10(abs(eig_anl(3)-eig_val(3) )); 
   

   //saving data
    ofstream file1("2numsol32.txt");
    ofstream file2("2anlsol.txt");
    ofstream file3("2eigloger.txt",std::ios_base::app);

    //Saving numerical solution
   // nodal locations to plot numerical solution (Eigenvectors)
/*    VectorXd X_loc1 = VectorXd::LinSpaced(n_nodes,0,1);
    for(int i=0; i<V1.size();i++){
      file1 << X_loc1(i) << std::setw(20) << V1new(i) << std::setw(20) << V2new(i) << std::setw(20) << V3new(i) << std::setw(20) << V4new(i) << endl;
    }
    
    //Saving EVP analytical solution
    for(int i=0; i<X_loc2.size();i++){
        file2 << X_loc2(i) << std::setw(20) << X_anl(i,0) << std::setw(20) << X_anl(i,1) << std::setw(20) << X_anl(i,2) << std::setw(20) << X_anl(i,3) << endl;
    }*/
   //Saving 1/#dof and eigvals errors
//	file3 << /*log10(0.1111) log10(0.0588)*log10(0.0303)*/log10(0.0154)<< std::setw(20) << lg_er1 << std::setw(20) << lg_er2 << std::setw(20) << lg_er3 <<std::setw(20) << lg_er4 << endl;

    file1.close();
    file2.close();
    file3.close();
    
}
