/*
*
* This code solves 4h order differential equation of the form d^4u/dx^4=f  0<x<1
* Using Hermite shape functions with m= 4, 8, 16,32 elements.
* 4 cases with different Boundary conditions and forces will be considered.
*
*/

// includes
#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <math.h>
#include "Eigen/Dense"
#include "Eigen/Eigen"

using namespace std;
using namespace Eigen;

int main() {

// Problem cases 
	int    c      = 3;  // 1 2 3 or 4 
    double k      = 1.0;
    double length = 1.0;
    
    VectorXd elem(4);
	elem << 4.0, 8.0, 16.0, 32.0;
	for (int n=0; n<elem.size(); n++){
		int n_elem = elem(n);
		int n_nodes = elem(n)+1;
		cout << n_elem << n_nodes << endl; 
	    double h       = length/ (n_nodes-1);
	
	    //element stiffness matrix
	    MatrixXd ke= MatrixXd::Zero(4,4);
	    ke.row(0) <<  6,    -6,   3*h,   3*h;
	    ke.row(1) << -6,     6,  -3*h,  -3*h;
	    ke.row(2) << 3*h, -3*h, 2*h*h,   h*h;
	    ke.row(3) << 3*h, -3*h,   h*h, 2*h*h;
	    ke *= 2*k/pow(h,3); 
	    //element force vector
	    VectorXd fe(4);
	    fe<<4,  4,  2*h/3,  -2*h/3;
	    fe *= h/8;
 
	    //Global matrices assembly
	    MatrixXd K_mat   = MatrixXd::Zero(2*n_nodes,2*n_nodes);
	    VectorXd F_vec   = VectorXd::Zero(2*n_nodes);
	    VectorXd X_vec   = VectorXd::Zero(2*n_nodes);
	    
	    //Global stiffness and force vector
	    for(int i_elem=0; i_elem <n_elem ;i_elem++){
	
	        MatrixXd K_mat_old_e1            = K_mat.block<2,2>(i_elem,i_elem);
	        K_mat_old_e1                     = K_mat_old_e1 + ke.topLeftCorner(2,2);
	        K_mat.block<2,2>(i_elem,i_elem)  = K_mat_old_e1;
	        
	        MatrixXd K_mat_old_e2            = K_mat.block<2,2>(i_elem,i_elem+n_nodes);
	        K_mat_old_e2                     = K_mat_old_e2 + ke.topRightCorner(2,2);
	        K_mat.block<2,2>(i_elem,i_elem+n_nodes) = K_mat_old_e2;
	        
	        MatrixXd K_mat_old_e3            = K_mat.block<2,2>(i_elem+n_nodes,i_elem);
	        K_mat_old_e3                     = K_mat_old_e3 + ke.bottomLeftCorner(2,2);
	        K_mat.block<2,2>(i_elem+n_nodes,i_elem) = K_mat_old_e3;
	        
	        MatrixXd K_mat_old_e4            = K_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes);
	        K_mat_old_e4                     = K_mat_old_e4 + ke.bottomRightCorner(2,2);
	        K_mat.block<2,2>(i_elem+n_nodes,i_elem+n_nodes) = K_mat_old_e4;
	
	        VectorXd F_vec_old_e1               = F_vec.segment<2>(i_elem);
	        F_vec_old_e1                        = F_vec_old_e1 + fe.head(2);
	        F_vec.segment<2>(i_elem) = F_vec_old_e1;
	        VectorXd F_vec_old_e2               = F_vec.segment<2>(i_elem+n_nodes);
	        F_vec_old_e2                        = F_vec_old_e2 + fe.tail(2);
	        F_vec.segment<2>(i_elem+n_nodes) = F_vec_old_e2;
	    }
	
	// problem cases 
	if (c==1){
		double f      = 2.0;
		double u0     = 0.0;
		double u1     = 0.0;
		double d2udx0 = 0.0;
		double d2udx1 = 0.0;
		
		F_vec *= f;
		
		// applying BC 
		F_vec(0)           = u0;
		F_vec(n_nodes-1)   = u1;
		F_vec(n_nodes)     = F_vec(n_nodes)+d2udx0;
		F_vec(2*n_nodes-1) = F_vec(2*n_nodes-1)+d2udx1;
		
	    K_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	    K_mat(0,0)         = 1;
	    K_mat.row(n_nodes-1)       = VectorXd::Zero(2*n_nodes); //u1
	    K_mat(n_nodes-1,n_nodes-1) = 1;
	
	    // solving the problem 
	    VectorXd X_vec = K_mat.lu().solve(F_vec);
	 
	 // saving the solution 
	 if (n_elem==4.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file1("c1n4.txt");
		file1 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file1.close();
	 }
	  if (n_elem==8.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file2("c1n8.txt");
		file2 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file2 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file2.close();
	 }
	  if (n_elem==16.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file3("c1n16.txt");
		file3 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file3 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file3.close();
	 } 
	 if (n_elem==32.0){
	 	ofstream file4("c1n32.txt");
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
		file4 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file4 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file4.close();
	 }
		
	}
	
	if (c==2 ){
		double f     = 2.0;
		double u0    = 0.0;
		double u1    = 0.0;
		double dudx0 = 0.0;
		double dudx1 = 0.0;
	
		F_vec *= f;
		// applying BC 
		F_vec(0)           = u0;
		F_vec(n_nodes-1)   = u1;
		F_vec(n_nodes)     = dudx0;
		F_vec(2*n_nodes-1) = dudx1;
		
		K_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	    K_mat(0,0)         = 1;
	    K_mat.row(n_nodes-1)          = VectorXd::Zero(2*n_nodes); //u1
	    K_mat(n_nodes-1,n_nodes-1)    = 1;
		K_mat.row(n_nodes)          = VectorXd::Zero(2*n_nodes); // dudx0
	    K_mat(n_nodes,n_nodes)      = 1;
	    K_mat.row(2*n_nodes-1)            = VectorXd::Zero(2*n_nodes); //dudx1
	    K_mat(2*n_nodes-1,2*n_nodes-1)    = 1;
	
	    VectorXd X_vec = K_mat.lu().solve(F_vec);
	
	 // saving the solution 
	 if (n_elem==4.0){
	 	ofstream file1("c2n4.txt");
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
		file1 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file1.close();
	 }
	  if (n_elem==8.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file2("c2n8.txt");
		file2 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file2 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file2.close();
	 }
	  if (n_elem==16.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file3("c2n16.txt");
		file3 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file3 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file3.close();
	 } 
	 if (n_elem==32.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file4("c2n32.txt");
		file4 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file4 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file4.close();
	 }
	}
	
	
	if (c==3){
		double f      = 3.0;
		double u0     = 0.0;
		double dudx1  = 0.0;
		double d2udx0 = 0.0;
		double d3udx1 = 0.0;
	
		F_vec *= f;
		// applying BC 
		F_vec(0)           = u0;
		F_vec(2*n_nodes-1) = dudx1;
		F_vec(n_nodes)     = F_vec(n_nodes)+d2udx0;
		F_vec(2*n_nodes-1) = F_vec(2*n_nodes-1)-d3udx1;
		
		K_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	    K_mat(0,0)         = 1;
	    K_mat.row(2*n_nodes-1)            = VectorXd::Zero(2*n_nodes); //dudx1
	    K_mat(2*n_nodes-1,2*n_nodes-1)    = 1;
	
	    VectorXd X_vec = K_mat.lu().solve(F_vec);
	    cout << "xvec" << endl<< X_vec << endl;
	 
	// saving the solution 
	 if (n_elem==4.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file1("c3n4.txt");
		file1 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file1.close();
	 }
	  if (n_elem==8.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file2("c3n8.txt");
		file2 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file2 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file2.close();
	 }
	  if (n_elem==16.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file3("c3n16.txt");
		file3 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file3 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file3.close();
	 } 
	 if (n_elem==32.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file4("c3n32.txt");
		file4 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file4 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file4.close();
	 }
	}
	
	
	if (c==4){
		double f      = 1.0;
		double u0     = 0.0;
		double u1     = 0.0;
		double d2udx0 = 0.0;
		double d2udx1 = 2.0; // -2.0
		F_vec *= f;
		// applying BC 
		F_vec(0)           = u0;
		F_vec(n_nodes-1)   = u1;
		F_vec(n_nodes)     = F_vec(n_nodes)+d2udx0;
		F_vec(2*n_nodes-1) = F_vec(2*n_nodes-1)-d2udx1;
		
		K_mat.row(0)       = VectorXd::Zero(2*n_nodes); //u0
	    K_mat(0,0)         = 1;
	    K_mat.row(n_nodes-1)          = VectorXd::Zero(2*n_nodes); //u1
	    K_mat(n_nodes-1,n_nodes-1)    = 1;
	
	    VectorXd X_vec = K_mat.lu().solve(F_vec);
	
	// saving the solution 
	 if (n_elem==4.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file1("c4n4.txt");
		file1 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file1 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file1.close();
	 }
	  if (n_elem==8.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file2("c4n8.txt");
		file2 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file2 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+n_nodes) << endl;
	        }
	 	file2.close();
	 }
	  if (n_elem==16.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file3("c4n16.txt");
		file3 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file3 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file3.close();
	 } 
	 if (n_elem==32.0){
	 	// nodal locations to plot numerical solution
	    VectorXd x_loc1 = VectorXd::LinSpaced((n_elem+1),0,1);
	 	ofstream file4("c4n32.txt");
		file4 << "x" << std::setw(20) << "u" << std::setw(20) <<"dudx"<< endl;
		for(int i=0; i<(n_elem+1);i++){
	            file4 << x_loc1(i) << std::setw(20) << X_vec(i) << std::setw(20) << X_vec(i+(n_elem+1)) << endl;
	        }
	 	file4.close();
	 }
} 
}
}    
